/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   display_file.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/05 09:21:02 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2019/03/19 07:34:51 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

static void	display_uidgid(int uid_f, int gid_f, t_dat *data)
{
	if (data->uid_name)
		ft_printf("%-?s%-?s", uid_f + 2, data->uid_name, gid_f, data->gid_name);
	else
		ft_printf("%-?d%-?d", uid_f + 2, data->uid, gid_f, data->gid);
}

static void	display_minmaj(t_maxs *maxs, int size, t_dat *data)
{
	int min_f;
	int	maj_f;

	min_f = maxs->uid;
	maj_f = maxs->gid;
	if (((S_IFMT & data->mode) == S_IFBLK) || (S_IFMT & data->mode) == S_IFCHR)
	{
		maxs->is_majmin = 1;
		ft_printf("%?d,%?d ", maj_f + 3 - 12, data->maj, min_f, data->min);
	}
	else if (maxs->is_majmin == 1)
	{
		if (maj_f > 1)
			ft_printf(" %?lld ", size + maj_f - 2 - 12, data->size);
		else
			ft_printf("%?lld ", size + 2 - 12, data->size);
	}
	else
	{
		if (maj_f > 1)
			ft_printf(" %?lld ", size + 3, data->size);
		else
			ft_printf("%?lld ", size + 2, data->size);
	}
}

void		display_file_dat_list(t_dat *data, t_maxs *maxs)
{
	time_t	today;

	if (ft_options("i", DECODE))
		ft_printf("%?llu ", maxs->ino, data->ino);
	ft_printf("%s ", data->att);
	ft_printf("%?ld ", maxs->nb_links, data->nb_links);
	display_uidgid(maxs->uid, maxs->gid, data);
	display_minmaj(maxs, maxs->size, data);
	ft_printf("%s", data->date_str);
	if (time(&today) - data->time > 15778800 ||
											data->time - time(&today) > 3600)
		ft_printf("  %s", data->year_str);
	else
		ft_printf("%s", data->hour_str);
	ft_printf(" %s ", data->name);
	if (data->link_src)
		ft_printf("-> %s", data->link_src);
	ft_printf("\n");
}

void		display_file_dat(t_dat *data, t_maxs *maxs)
{
	if (ft_options("i", DECODE))
		ft_printf("%?llu ", maxs->ino, data->ino);
	ft_printf("%s\n", data->name);
}
