## ft_ls

![purpose](https://img.shields.io/badge/purpose-education-green.svg)
![purpose](https://img.shields.io/badge/42-School-Blue.svg)


Simple reproduction of the [ls command](https://man7.org/linux/man-pages/man1/ls.1.html).

Support `-l`, `-R`, `-a`, `-r` and `-t` options.

