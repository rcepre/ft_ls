# **************************************************************************** #
#                                                           LE - /             #
#                                                               /              #
#    Makefile                                         .::    .:/ .      .::    #
#                                                  +:+:+   +:    +:  +:+:+     #
#    By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+      #
#                                                  #+#   #+    #+    #+#       #
#    Created: 2018/11/07 09:01:58 by rcepre       #+#   ##    ##    #+#        #
#    Updated: 2019/03/12 06:42:03 by rcepre      ###    #+. /#+    ###.fr      #
#                                                          /                   #
#                                                         /                    #
# **************************************************************************** #

NAME = ft_ls

CC= clang

CFLAGS= -Wall -Wextra  -g #-Werror

SRC = 				main.c \
					struct_utilities.c \
					get_data.c \
					get_data_two.c \
					add_elem.c \
					utilities.c \
					display.c \
					display_file.c \
					errors.c \
					

HEADER = ./ft_ls.h

OBJECT = $(SRC:.c=.o)

all: $(NAME)

LIBFT_PATH = ./libft/

LIBFT = libft.a

####################################TEXT########################################
	RED = \033[1;31m
	WHITE = \033[0;29m
	YELLOW = \033[1;33m
	PINK = \033[1;35m

	OK  = \033[1;32m[OK]\n
	RM_OBJ = "$(YELLOW)push_swap:\t$(RED)rm $(YELLOW)objects\t\t$(WHITE)"
	RM_LIB_OBJ = "$(YELLOW)libft:\t$(RED)rm $(YELLOW)objects\t\t$(WHITE)"
	RM_LIB = "\t$(PINK)rm libft.a\t\)$(WHITE)"
	RM_EXEC1 = "$(YELLOW)push_swap:\t$(RED)rm $(YELLOW)push_swap\t\t$(WHITE)"
	LINK = "\t$(PINK)$(NAME)...\t\t$(WHITE)\n"
	COMPILING_LIB = "$(YELLOW)libft:\t\t$(YELLOW)libft.a building\t$(GREEN)$(WHITE)"

####################################RULES######################################

$(NAME): $(LIBFT) $(OBJECT) $(HEADER)
	@$(CC) $(CFLAGS) $(OBJECT) $(LIBFT_PATH)libft.a -I $< -L $(LIBFT_PATH) -lft -o $(NAME)
	@printf "\t\t\t\t\t$(OK)"

$(LIBFT):
	@echo $(COMPILING_LIB)
	@echo "\t\033[0;36m#########$(RED)libft\033[0;36m#########"
	@(cd $(LIBFT_PATH) && make)
	@printf "\t\t\t\t\t$(OK)"
	@printf $(LINK)

%.o: %.c
	@printf "\033[0;36m>>>\tCompiling: $? ..."
	@$(CC) -c $(CFLAGS) -I $(HEADER) $?
	@printf "   [ok]$(WHITE)\n"

.PHONY: clean fclean

clean:
	@(cd $(LIBFT_PATH) && make clean)
	@printf $(RM_OBJ)
	@rm -rf $(OBJECT)
	@printf "$(OK)"

fclean: clean
	@(cd $(LIBFT_PATH) && make clean_exe)
	@printf $(RM_EXEC1)
	@printf "$(OK)"
	@rm -rf $(NAME)
	@printf "$(OK)"

re: fclean all
