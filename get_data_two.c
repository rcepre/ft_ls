/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   get_data_two.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/29 07:12:26 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2019/03/18 10:32:48 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

static void	compare_maxs(t_dat *data, t_maxs *maxs)
{
	int		tmp;

	tmp = (int)ft_strlen(data->gid_name);
	maxs->gid = tmp > maxs->gid ? tmp : maxs->gid;
	tmp = (int)ft_strlen(data->uid_name);
	maxs->uid = tmp > maxs->uid ? tmp : maxs->uid;
	tmp = (int)ft_lllen(data->nb_links, 10);
	maxs->nb_links = tmp > maxs->nb_links ? tmp : maxs->nb_links;
	tmp = (int)ft_lllen(data->size, 10);
	maxs->size = tmp > maxs->size ? tmp : maxs->size;
	tmp = (int)ft_lllen(data->ino, 10);
	maxs->ino = tmp > maxs->ino ? tmp : maxs->ino;
	tmp = (int)ft_lllen(data->maj, 10);
	maxs->maj = tmp > maxs->maj ? tmp : maxs->maj;
	tmp = (int)ft_lllen(data->min, 10);
	maxs->min = tmp > maxs->min ? tmp : maxs->min;
	if (data->name[0] != '.' || ft_options("a", DECODE))
		maxs->nb_blocks += data->nb_blocks;
}

t_maxs		get_maxs(t_dat *data)
{
	t_maxs	maxs;
	int		i;

	i = lst_dat_size(data) + 1;
	maxs = init_maxs();
	while (i--)
	{
		compare_maxs(data, &maxs);
		data = data->next;
	}
	return (maxs);
}

void		get_file_type(unsigned long mode, t_dat *data)
{
	if ((S_IFMT & mode) == S_IFLNK)
		data->att[0] = 'l';
	if ((S_IFMT & mode) == S_IFREG)
		data->att[0] = '-';
	if ((S_IFMT & mode) == S_IFBLK)
		data->att[0] = 'b';
	if ((S_IFMT & mode) == S_IFDIR)
		data->att[0] = 'd';
	if ((S_IFMT & mode) == S_IFCHR)
		data->att[0] = 'c';
	if ((S_IFMT & mode) == S_IFIFO)
		data->att[0] = 'p';
	if ((S_IFMT & mode) == S_IFSOCK)
		data->att[0] = 's';
}
