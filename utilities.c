/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   utilities.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/05 09:21:52 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2019/03/12 06:15:00 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

static char	**sort_args_date(char **av, int nb)
{
	int			sorted;
	int			i;
	char		*tmp;
	struct stat	buf;
	struct stat	buf_vs;

	sorted = 0;
	while (!sorted)
	{
		sorted = 1;
		i = 1;
		while (++i < nb)
		{
			stat(av[i], &buf);
			stat(av[i - 1], &buf_vs);
			if (av[i - 1][0] != '-' && buf.st_mtime > buf_vs.st_mtime)
			{
				tmp = av[i];
				av[i] = av[i - 1];
				av[i - 1] = tmp;
				sorted = 0;
			}
		}
	}
	return (av);
}

char		**sort_args(char **av, int nb)
{
	int		sorted;
	int		i;
	char	*tmp;

	sorted = 0;
	while (!sorted)
	{
		sorted = 1;
		i = 1;
		while (++i < nb)
		{
			if (av[i - 1][0] != '-' && ft_strcmp(av[i], av[i - 1]) < 0)
			{
				tmp = av[i];
				av[i] = av[i - 1];
				av[i - 1] = tmp;
				sorted = 0;
			}
		}
	}
	if (ft_options("t", DECODE))
		sort_args_date(av, nb);
	return (av);
}

char		*get_path(char *path, char *name)
{
	char	*full_path;
	char	*tmp;

	if (!(tmp = ft_strjoin(path, "/")))
		return (NULL);
	if (!(full_path = ft_strjoin(tmp, name)))
		return (NULL);
	free(tmp);
	return (full_path);
}
