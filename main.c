/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/28 21:33:44 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2019/03/19 07:37:39 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

int		recurse(t_dat *data, char *path)
{
	int		i;
	t_dat	*start;

	i = lst_dat_size(data) + 1;
	start = data;
	if (ft_options("r", DECODE))
		while (data->next)
			data = data->next;
	while (i--)
	{
		if ((S_IFMT & data->mode) == S_IFDIR && ft_strcmp(data->name, ".") &&
													ft_strcmp(data->name, ".."))
			ft_ls(get_path(path, data->name), 1);
		if (ft_options("r", DECODE))
			if (data->prev)
				data = data->prev;
		if (!ft_options("r", DECODE))
			if (data->next)
				data = data->next;
	}
	data = start;
	return (1);
}

int		ft_ls(char *path, int nb)
{
	DIR				*dir;
	t_dat			*data;
	t_dat			*elem;
	struct dirent	*dirent;

	if (!(dir = opendir(path)))
	{
		check_dir_error(path);
		free(path);
		return (0);
	}
	data = NULL;
	while ((dirent = readdir(dir)))
	{
		if (!(elem = get_file_data(dirent, get_path(path, dirent->d_name))))
			alloc_error(&data, &path, 1);
		data = add_elem(data, elem);
	}
	display_dat_list(data, path, nb);
	if (ft_options("R", DECODE))
		recurse(data, path);
	free_list(&data);
	free(path);
	closedir(dir);
	return (1);
}

int		get_files(int i, int ac, char **av)
{
	struct stat		buf;
	t_dat			*data;
	t_dat			*elem;

	i -= 1;
	data = NULL;
	while (++i < ac)
	{
		if ((stat(av[i], &buf) != -1))
		{
			if ((S_IFMT & buf.st_mode) == S_IFREG)
			{
				if (!(elem = get_file_data(NULL, ft_strdup(av[i]))))
					alloc_error(&data, NULL, 1);
				data = add_elem(data, elem);
			}
		}
	}
	display_dat_list(data, ".", i);
	free_list(&data);
	return (1);
}

int		main(int ac, char **av)
{
	int i;

	errno = 0;
	if ((i = ft_options_encode(av, ac, "Ralrti")) < 0)
		ft_option_error(i, "ft_ls", "Ralrti");
	check_file_errors(av, ac);
	sort_args(av, ac);
	get_files(i, ac, av);
	if (i == ac)
		ft_ls(ft_strdup("."), ac - i);
	else
		while (i < ac)
		{
			ft_ls(ft_strdup(av[i]), ac - i);
			i++;
		}
	return (0);
}
