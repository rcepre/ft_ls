/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_options.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/03/05 06:42:26 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2019/03/12 13:53:12 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

int		ft_options(char *str, int instruction)
{
	static uint64_t		options = 0;
	uint64_t			code;
	int					i;

	i = -1;
	if (instruction == ENCODE)
	{
		while (str[++i])
		{
			code = ft_isdigit(str[i]) ? str[i] + 4 : 0;
			code = str[i] - (ft_isupper(str[i]) ? 65 : 71);
			options = options | 1LU << code;
		}
	}
	if (instruction == DECODE)
	{
		while (ft_isalnum(str[++i]))
		{
			code = ft_isdigit(str[i]) ? str[i] + 4 : 0;
			code = str[i] - (ft_isupper(str[i]) ? 65 : 71);
		}
		return (!!(options & (1LU << code)));
	}
	return (0);
}

void	ft_option_error(char c, char *name, char *valids)
{
	ft_printf("%s: illegal option -- %c\n", name, c * -1);
	ft_printf("usage: ./%s [-%s] [file ...]\n", name, valids);
	exit(0);
}

char	ft_options_encode(char **av, int ac, char *valids)
{
	int			i;
	int			j;
	int			k;
	char		str[62];

	j = 0;
	i = 0;
	k = -1;
	ft_bzero(str, 62);
	while (++j < ac && av[j][0] == '-' && ft_strcmp(av[j], "--"))
	{
		i = 0;
		while ((av[j][++i]))
		{
			if (!ft_strchr(valids, av[j][i]))
				return (av[j][i] * -1);
			str[++k] = av[j][i];
		}
	}
	if (av[j] && !ft_strcmp(av[j], "--"))
		j++;
	ft_options(str, ENCODE);
	return (j);
}
