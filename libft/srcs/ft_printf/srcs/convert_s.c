/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   convert_s.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/12/02 17:08:45 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2018/12/11 15:47:16 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libftprintf.h"

int				convert_s(char *s, t_arg *arg)
{
	int		f_size;
	int		s_len;

	if (!s && arg->pre)
		s = "(null)";
	else if (!s || arg->pre == 0)
		s = "";
	if (arg->pre < 0 || !s[0])
		arg->pre = 0;
	s_len = arg->pre && arg->pre <= (int)ft_strlen(s) ? arg->pre : ft_strlen(s);
	if (arg->field)
		f_size = arg->field > s_len ? arg->field : s_len;
	else
		f_size = s_len;
	if (!(arg->data = ft_strnew(f_size)))
		return (-1);
	ft_memset(arg->data, arg->zero_fill ? '0' : ' ', f_size);
	if (!arg->left)
		ft_memcpy(arg->data + (f_size - s_len), s, s_len);
	if (arg->left)
		ft_memcpy(arg->data, s, s_len);
	ft_putstr(arg->data);
	return (f_size);
}
