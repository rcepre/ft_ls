/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   convert_d.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/22 09:32:44 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2018/12/11 15:37:50 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libftprintf.h"

static void			put_nb_in_field(t_arg *arg, int i, char *field)
{
	char			sign;
	int				nb_len;

	if ((arg->space && !arg->left) && !arg->zero_fill)
		i -= 1;
	nb_len = ft_strlen(arg->data);
	sign = arg->neg_sign ? '-' : 0;
	sign = arg->pos_sign ? '+' : sign;
	sign = arg->space ? ' ' : sign;
	if (sign)
	{
		if (arg->zero_fill)
		{
			field[0] = sign;
			i += (arg->neg_sign || arg->pos_sign) ? 1 : 0;
		}
		if (!arg->zero_fill)
			field[i++] = sign;
	}
	if (arg->pre)
		while (arg->pre-- - nb_len > 0)
			field[i++] = '0';
	ft_memcpy(field + i, arg->data, nb_len);
	free(arg->data);
	arg->data = field;
}

static int			create_field(t_arg *arg, char **field)
{
	int				f_size;
	int				nb_size;

	nb_size = dsize(arg);
	f_size = arg->field > nb_size ? arg->field : nb_size + arg->space;
	if (!(*field = ft_strnew(f_size)))
		return (-1);
	ft_memset(*field, arg->zero_fill ? '0' : ' ', f_size);
	return (f_size);
}

static long long	cast_from_modifier(long long n, t_arg *arg)
{
	if (!arg->mod)
		return ((int)n);
	if (!ft_strcmp(arg->mod, "hh"))
		return ((char)n);
	if (!ft_strcmp(arg->mod, "h"))
		return ((short int)n);
	if (!ft_strcmp(arg->mod, "l"))
		return ((long int)n);
	return (n);
}

int					convert_d(long long n, t_arg *arg)
{
	char			*field;
	int				size;

	n = cast_from_modifier(n, arg);
	n = check_neg(n, arg);
	if (n < -FT_LONG_MAX)
		return (ft_putstr("-9223372036854775808"));
	if (!(arg->data = ft_lltoa(n)))
		return (-1);
	if ((size = create_field(arg, &field)) == -1)
		return (-1);
	if (n == 0 && arg->pre == 0)
		return (size = put_empty_field(field, size));
	else
	{
		put_nb_in_field(arg, (arg->left ? 0 : size - dsize(arg)), field);
		return (ft_putstr(arg->data));
	}
}
