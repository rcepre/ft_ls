/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   get_arg_data.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/22 07:18:30 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2019/02/04 19:46:30 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libftprintf.h"

void			filter_arg_data(t_arg *arg)
{
	if (arg->pre == -1 && arg->type == 'f')
		arg->pre = 6;
	if (arg->left || (arg->pre > 0 && arg->type != 'f'))
		arg->zero_fill = 0;
	if (arg->type == 'd' || arg->type == 'u' || (arg->type == 'f' && arg->pre))
		arg->hash = 0;
	if (arg->pos_sign)
		arg->space = 0;
	if (arg->neg_sign == 1)
		arg->pos_sign = 0;
	if ((arg->type == 'd' || arg->type == 'i' || arg->type == 'o' ||\
	arg->type == 'u' || arg->type == 'x' || arg->type == 'X') && arg->pre == 0)
		arg->zero_fill = 0;
}

int				get_data(va_list va, t_arg *arg)
{
	int ret;

	if (arg->type == 'd' || arg->type == 'i')
		ret = convert_d(va_arg(va, long long), arg);
	if (arg->type == '%')
		ret = convert_percent(arg);
	if (arg->type == 'u' || arg->type == 'x' || arg->type == 'X' ||\
										arg->type == 'o' || arg->type == 'b')
		ret = convert_u(va_arg(va, unsigned long long), arg);
	if (arg->type == 'f')
	{
		if (arg->mod == NULL || !ft_strcmp(arg->mod, "l"))
			ret = convert_f(va_arg(va, double), arg);
		else if (arg->mod)
			ret = convert_f(va_arg(va, long double), arg);
	}
	if (arg->type == 'c')
		ret = convert_c(va_arg(va, wchar_t), arg);
	if (arg->type == 's')
		ret = convert_s(va_arg(va, char*), arg);
	if (arg->type == 'p')
		ret = convert_p(va_arg(va, int64_t), arg);
	return (ret);
}

int				get_arg_data(va_list va, t_arg *arg, char **str)
{
	int		len;
	char	*tmp;

	len = get_type(*str, arg);
	if (!(tmp = ft_strndup(*str, len)))
		return (-1);
	get_field(tmp, arg, va);
	get_pre(tmp, arg, va);
	get_modifier(arg, tmp);
	get_attibutes(tmp, arg, len);
	filter_arg_data(arg);
	free(tmp);
	*str += len - 1;
	return (get_data(va, arg));
}
