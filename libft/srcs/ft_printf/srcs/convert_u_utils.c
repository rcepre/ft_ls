/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   convert_u_utils.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/22 09:32:44 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2018/12/11 17:23:01 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libftprintf.h"

int				usize(t_arg *arg)
{
	int nb_size;

	nb_size = ft_strlen(arg->data);
	if (arg->pre > nb_size)
		nb_size = arg->pre;
	return (nb_size);
}

int				get_prefix_size(int n, t_arg *arg)
{
	if (arg->hash)
	{
		if ((arg->type == 'x' || arg->type == 'X') && n)
			return (2);
		if ((arg->type == 'o' && arg->hash) || \
								(arg->type == 'o' && arg->pre == 0 && n == 0))
			return (1);
	}
	return (0);
}

int				get_base(t_arg *arg)
{
	int base;

	base = 10;
	if (arg->type == 'x' || arg->type == 'X')
		base = 16;
	else if (arg->type == 'o')
		base = 8;
	else if (arg->type == 'b')
		base = 2;
	return (base);
}
