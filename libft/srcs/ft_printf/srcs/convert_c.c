/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   convert_c.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/12/02 17:08:45 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2018/12/11 16:20:09 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libftprintf.h"

static wchar_t		cast_from_modifier(wchar_t c, t_arg *arg)
{
	if (arg->mod && !ft_strcmp(arg->mod, "l"))
		return (c);
	return ((char)c);
}

int					convert_c(wchar_t c, t_arg *arg)
{
	int		f_size;

	f_size = arg->field ? arg->field - 1 : 0;
	c = cast_from_modifier(c, arg);
	if (!(arg->data = ft_strnew(f_size)))
		return (-1);
	ft_memset(arg->data, arg->zero_fill ? '0' : ' ', f_size);
	if (arg->left)
	{
		write(1, &c, 1);
		ft_putstr(arg->data);
	}
	else if (!arg->left)
	{
		ft_putstr(arg->data);
		write(1, &c, 1);
	}
	free(arg->data);
	arg->data = NULL;
	return (f_size + 1);
}
