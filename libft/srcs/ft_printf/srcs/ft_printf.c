/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_printf.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/04 19:47:50 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2019/02/04 19:47:53 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libftprintf.h"

int				skip_text(char **format)
{
	int i;

	i = 0;
	while ((*format)[i] && (*format)[i] != '%')
		i++;
	write(1, *format, i);
	*(format) += i;
	return (i);
}

int				ft_printf(char *format, ...)
{
	va_list		va;
	t_arg		*arg;
	int			written;
	int			error;
	int			ret;

	written = 0;
	error = 0;
	va_start(va, format);
	while (*format)
	{
		written += skip_text(&format);
		if (*format && *format == '%')
		{
			arg = init_t_arg();
			if ((ret = get_arg_data(va, arg, &format)) == -1)
				error = 1;
			written += ret;
			free(arg->data);
			free(arg);
			format++;
		}
	}
	va_end(va);
	return (error ? -1 : written);
}
