/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   convert_d_utils.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/22 09:32:44 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2018/12/11 15:36:44 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libftprintf.h"

int					dsize(t_arg *arg)
{
	int				nb_size;

	nb_size = ft_strlen(arg->data);
	if (arg->pre > nb_size)
		nb_size = arg->pre + arg->neg_sign + arg->pos_sign;
	else if (arg->pre <= nb_size)
		nb_size = nb_size + arg->neg_sign + arg->pos_sign;
	if ((arg->field > nb_size && !arg->zero_fill) && !arg->left)
		arg->space = 0;
	return (nb_size);
}

long long			check_neg(long long n, t_arg *arg)
{
	if (n < 0)
	{
		n *= -1;
		arg->neg_sign = 1;
		arg->space = 0;
		arg->pos_sign = 0;
	}
	return (n);
}

int					put_empty_field(char *field, int size)
{
	int				ret;

	if (size < 2)
		ret = ft_putstr("");
	else
		ret = ft_putstr(field);
	free(field);
	return (ret);
}
