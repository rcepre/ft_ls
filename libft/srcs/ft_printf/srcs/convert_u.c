/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   convert_u.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/22 09:32:44 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2018/12/12 16:56:59 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libftprintf.h"

static void				put_nb_in_field(t_arg *arg, int i, char *field, int n)
{
	int j;
	int pref;
	int nb_len;

	nb_len = ft_strlen(arg->data);
	pref = get_prefix_size(n, arg);
	j = arg->left || arg->zero_fill ? 0 : i - pref;
	if (pref && (arg->hash && !(arg->type == 'o' && arg->pre == 0)))
	{
		if (arg->type == 'x' || arg->type == 'X' || arg->type == 'o')
		{
			field[j++] = '0';
			if (arg->type == 'x' || arg->type == 'X')
				field[j++] = 'x';
		}
	}
	if (arg->left)
		i += j;
	while (arg->pre-- - nb_len > 0)
		field[i++] = '0';
	ft_memcpy(field + i, arg->data, nb_len);
	free(arg->data);
	arg->data = field;
}

static int				create_field(t_arg *arg, char **field, int n)
{
	int		f_size;
	int		pref;

	pref = get_prefix_size(n, arg);
	f_size = arg->field > usize(arg) + pref ? arg->field : usize(arg) + pref;
	if ((arg->type == 'x' || arg->type == 'X' || arg->type == 'u' || \
							(arg->type == 'o' && arg->hash)) && arg->pre == 0)
		f_size -= arg->field ? 0 : 1;
	if (!arg->field && n == 0 && arg->pre == 0 && !arg->hash)
		f_size = 0;
	if (!(*field = ft_strnew(f_size)))
		return (-1);
	ft_memset(*field, arg->zero_fill ? '0' : ' ', f_size);
	return (f_size);
}

static size_t			cast_from_modifier(size_t n, t_arg *arg)
{
	if (!arg->mod)
		return ((unsigned int)n);
	else if (!ft_strcmp(arg->mod, "hh"))
		return ((unsigned char)n);
	else if (!ft_strcmp(arg->mod, "h"))
		return ((unsigned short int)n);
	else if (!ft_strcmp(arg->mod, "l"))
		return ((unsigned long int)n);
	return (n);
}

int						convert_u(size_t n, t_arg *arg)
{
	char				*field;
	int					size;
	unsigned long long	base;

	if ((n == 0 && arg->type != 'u' && arg->pre))
		arg->hash = 0;
	base = get_base(arg);
	n = cast_from_modifier(n, arg);
	arg->data = ft_ulltoa(n, base);
	if (((size = create_field(arg, &field, n))) == -1)
		return (-1);
	if ((arg->pre == 0 && n == 0) && (arg->type != 'o' || !arg->hash))
	{
		size = ft_putstr(field);
		free(field);
		return (size);
	}
	put_nb_in_field(arg, (arg->left ? 0 : size - usize(arg)), field, n);
	if (arg->type == 'X')
		ft_strupcase(arg->data);
	ft_putstr(arg->data);
	return (size);
}
