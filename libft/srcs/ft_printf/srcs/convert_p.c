/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   convert_p.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/12/02 17:08:45 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2018/12/17 14:00:24 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libftprintf.h"

int				special_cases(t_arg *arg)
{
	int		f_size;
	char	*prefix;
	int		pre_len;

	prefix = arg->pre == -1 ? "0x0" : "0x";
	pre_len = ft_strlen(prefix);
	f_size = arg->field > pre_len ? arg->field : pre_len;
	f_size = arg->pre > f_size ? arg->pre + pre_len : f_size;
	if (!(arg->data = ft_strnew(f_size)))
		return (-1);
	ft_memset(arg->data, arg->zero_fill ? '0' : ' ', f_size);
	if (arg->left || arg->zero_fill)
		ft_memcpy(arg->data, prefix, pre_len);
	else if (!arg->left)
		ft_memcpy(arg->data + (f_size - pre_len), prefix, pre_len);
	ft_putstr(arg->data);
	return (f_size);
}

int				convert_p(uint64_t ptr, t_arg *arg)
{
	arg->type = 'x';
	arg->mod = "l";
	arg->hash = 1;
	if (ptr == 0 && arg->pre > 0)
	{
		arg->left = 1;
		arg->zero_fill = 1;
	}
	if (ptr)
		return (convert_u(ptr, arg));
	else
		return (special_cases(arg));
}
