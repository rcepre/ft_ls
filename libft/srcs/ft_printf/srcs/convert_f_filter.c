/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   convert_f_filter.c                               .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/13 17:50:59 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2018/12/18 18:08:30 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libftprintf.h"

static int				put_error_in_field(t_arg *arg)
{
	int		s_len;
	char	*field;
	int		size;

	if (!arg->data)
		return (0);
	s_len = ft_strlen(arg->data);
	size = arg->field > s_len ? arg->field : s_len;
	if (!(field = ft_strnew(size)))
		return (0);
	ft_memset(field, ' ', size);
	if (arg->left)
		ft_memcpy(field, arg->data, s_len);
	if (!arg->left)
		ft_memcpy(field + (size - s_len), arg->data, s_len);
	free(arg->data);
	arg->data = ft_strdup(field);
	free(field);
	return (1);
}

static long double		flt_check_neg(long double nb, t_arg *arg)
{
	if (nb < 0)
	{
		nb *= -1;
		arg->neg_sign = 1;
	}
	return (nb);
}

static int				check_nan_inf(long double nb)
{
	uint64_t *tmp;
	uint64_t si;
	uint64_t mant;
	uint64_t exposant;

	tmp = (uint64_t*)&nb;
	si = (tmp[0] >> 62);
	mant = (tmp[0] << 2) >> 2;
	exposant = tmp[1] & 0x7FFF;
	if (exposant == 0x7FFF)
	{
		if (si == 0x0)
			return (mant == 0x0 ? 1 : 2);
		else if (si == 0x2)
			return (mant == 0x0 ? 1 : 2);
		else if (si == 0x1 || si == 0x3)
			return (2);
	}
	return (0);
}

int						filter(long double *nb, t_arg *arg)
{
	int ret;

	*nb = flt_check_neg(*nb, arg);
	ret = check_nan_inf(*nb);
	if (ret == 1 && arg->neg_sign)
	{
		if (!(arg->data = ft_strdup("-inf")))
			return (-1);
	}
	else if (ret == 1 && !arg->neg_sign)
	{
		if (!(arg->data = ft_strdup("inf")))
			return (-1);
	}
	else if (ret == 2)
	{
		if (!(arg->data = ft_strdup("nan")))
			return (-1);
	}
	if (arg->data)
		if (!(put_error_in_field(arg)))
			return (-1);
	return (arg->data ? 1 : 0);
}
