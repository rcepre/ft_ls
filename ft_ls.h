/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_ls.h                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/14 06:49:13 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2019/03/19 07:36:01 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include <stdio.h>
# include <string.h>
# include "libft/includes/libft.h"
# include <dirent.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <sys/xattr.h>
# include <unistd.h>
# include <time.h>
# include <pwd.h>
# include <grp.h>
# include <sys/acl.h>
# include <errno.h>

# define FORWARD 0
# define BACKWARD 1
# define SORT_ASCII 0
# define SORT_DATE 1

typedef struct		s_dat
{
	char			*att;
	char			*uid_name;
	char			*name;
	char			*gid_name;
	char			*date_str;
	char			*year_str;
	char			*hour_str;
	char			*link_src;
	int				time;
	int				ino;
	int				uid;
	int				gid;
	int				mode;
	int				size;
	int				min;
	int				maj;
	int				nb_links;
	int				nb_blocks;
	struct s_dat	*next;
	struct s_dat	*prev;
}					t_dat;

typedef struct		s_maxs
{
	int				nb_links;
	int				uid;
	int				gid;
	int				size;
	int				min;
	int				maj;
	int				nb_blocks;
	int				ino;
	int				is_majmin;

}					t_maxs;

void				display_file_dat_list(t_dat *data, t_maxs *maxs);
void				display_file_dat(t_dat *data, t_maxs *maxs);
t_maxs				get_maxs(t_dat *data);
void				get_file_type(unsigned long mode, t_dat *data);
void				alloc_error(t_dat **data, char **path, int exit);
char				**sort_args(char **av, int nb);
char				*get_path(char *path, char *name);
void				option_error(char c);
void				check_file_errors(char **av, int ac);
void				display_file_dat_list(t_dat *data, t_maxs *maxs);
void				display_dat_list(t_dat *data, char *path, int nb);
t_maxs				get_maxs(t_dat *data);
int					ft_ls(char	*path, int nb);
t_maxs				init_maxs();
void				free_list(t_dat **data);
t_dat				*get_file_data(struct dirent *dir, char *path);
t_dat				*new_dat();
t_dat				*add_elem(t_dat *data, t_dat *elem);
int					lst_dat_size(t_dat *data);
void				check_dir_error(char *path);

#endif
