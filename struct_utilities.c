/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   struct_utilities.c                               .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/28 21:33:44 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2019/03/12 06:13:40 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

t_maxs	init_maxs(void)
{
	t_maxs maxs;

	maxs.nb_links = 0;
	maxs.uid = 0;
	maxs.gid = 0;
	maxs.ino = 0;
	maxs.size = 0;
	maxs.maj = 0;
	maxs.min = 0;
	maxs.nb_blocks = 0;
	return (maxs);
}

int		lst_dat_size(t_dat *data)
{
	int i;

	i = 0;
	if (!data)
		return (0);
	while (data->next)
	{
		i++;
		data = data->next;
	}
	return (i);
}

t_dat	*new_dat(void)
{
	t_dat	*data;

	if (!(data = (t_dat*)malloc(sizeof(t_dat))))
		return (NULL);
	if (!(data->att = ft_strnew(11)))
	{
		free(data);
		return (NULL);
	}
	data->uid_name = NULL;
	data->name = NULL;
	data->gid_name = NULL;
	data->date_str = NULL;
	data->year_str = NULL;
	data->hour_str = NULL;
	data->link_src = NULL;
	data->nb_links = 0;
	data->nb_blocks = 0;
	data->next = NULL;
	data->prev = NULL;
	return (data);
}

void	free_list(t_dat **data)
{
	t_dat *tmp;

	while (*data)
	{
		if ((*data)->att)
			free((*data)->att);
		if ((*data)->name)
			free((*data)->name);
		if ((*data)->year_str)
			free((*data)->year_str);
		if ((*data)->date_str)
			free((*data)->date_str);
		if ((*data)->hour_str)
			free((*data)->hour_str);
		if ((*data)->link_src)
			free((*data)->link_src);
		if ((*data)->uid_name)
			free((*data)->uid_name);
		if ((*data)->gid_name)
			free((*data)->gid_name);
		tmp = *data;
		*data = (*data)->next;
		free(tmp);
	}
}
