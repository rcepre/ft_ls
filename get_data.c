/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   get_data.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/29 07:12:26 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2019/03/19 07:10:01 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

static void	get_file_rights(unsigned long mode, t_dat *data, char *path)
{
	acl_t tmp;

	data->att[1] = (S_IRUSR & mode) ? 'r' : '-';
	data->att[2] = (S_IWUSR & mode) ? 'w' : '-';
	data->att[3] = (S_IXUSR & mode) ? 'x' : '-';
	if (S_ISUID & mode)
		data->att[3] = data->att[3] == '-' ? 'S' : 's';
	data->att[4] = (S_IRGRP & mode) ? 'r' : '-';
	data->att[5] = (S_IWGRP & mode) ? 'w' : '-';
	data->att[6] = (S_IXGRP & mode) ? 'x' : '-';
	if (S_ISGID & mode)
		data->att[6] = data->att[6] == '-' ? 'S' : 's';
	data->att[7] = (S_IROTH & mode) ? 'r' : '-';
	data->att[8] = (S_IWOTH & mode) ? 'w' : '-';
	data->att[9] = (S_IXOTH & mode) ? 'x' : '-';
	if (S_ISVTX & mode)
		data->att[9] = data->att[9] == '-' ? 'T' : 't';
	data->att[11] = '\0';
	tmp = acl_get_file(path, ACL_TYPE_EXTENDED);
	data->att[10] = (tmp) ? '+' : ' ';
	data->att[10] = listxattr(path, NULL, 0, 1) > 0 ? '@' : data->att[10];
	free(tmp);
}

static int	get_pwuid_grgid(t_dat *data)
{
	struct passwd	*pwd;
	struct group	*grp;

	pwd = getpwuid(data->uid);
	grp = getgrgid(data->gid);
	if (pwd)
	{
		if (!(data->uid_name = ft_strdup(pwd->pw_name)))
			return (0);
	}
	else
		data->uid_name = NULL;
	if (grp)
	{
		if (!(data->gid_name = ft_strdup(grp->gr_name)))
			return (0);
	}
	else
		data->uid_name = NULL;
	return (1);
}

static int	get_others(t_dat *data, struct stat buf, struct dirent *dir,
																	char *path)
{
	if (!dir)
		data->name = ft_strdup(path);
	else
		data->name = ft_strdup(dir->d_name);
	data->gid = buf.st_gid;
	data->uid = buf.st_uid;
	data->mode = buf.st_mode;
	data->size = buf.st_size;
	data->date_str = ft_strsub(ctime(&buf.st_mtime), 4, 6);
	data->year_str = ft_strsub(ctime(&buf.st_mtime), 20, 4);
	data->hour_str = ft_strsub(ctime(&buf.st_mtime), 10, 6);
	data->time = buf.st_mtime;
	data->ino = buf.st_ino;
	data->nb_links = buf.st_nlink;
	data->min = (buf.st_rdev & 0xFFFF);
	data->maj = (buf.st_rdev >> 24);
	data->nb_blocks = buf.st_blocks;
	if (!data->name || !data->date_str || !data->year_str || !data->hour_str)
		return (0);
	return (1);
}

int			get_link(char *path, int mode, t_dat *data)
{
	char			*link;

	link = NULL;
	if (!(link = ft_strnew(256)))
		return (0);
	if ((S_IFMT & mode) == S_IFLNK)
	{
		readlink(path, link, 256);
		if (!(data->link_src = ft_strdup(link)))
			return (0);
	}
	free(link);
	return (1);
}

t_dat		*get_file_data(struct dirent *dir, char *path)
{
	struct stat		buf;
	t_dat			*data;

	data = NULL;
	if (!path || !(data = new_dat()))
	{
		alloc_error(&data, &path, 0);
		return (NULL);
	}
	lstat(path, &buf);
	if (!(get_link(path, buf.st_mode, data)) ||
				!(get_others(data, buf, dir, path)) || !(get_pwuid_grgid(data)))
	{
		alloc_error(&data, &path, 0);
		return (NULL);
	}
	get_file_type(data->mode, data);
	get_file_rights(data->mode, data, path);
	free(path);
	return (data);
}
