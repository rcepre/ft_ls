/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   errors.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/05 09:22:46 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2019/03/12 06:49:09 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

void		alloc_error(t_dat **data, char **path, int ins)
{
	if (*data)
		free_list(data);
	if (*path)
		free(*path);
	if (ins)
		exit(0);
}

void		option_error(char c)
{
	ft_printf("ft_ls: illegal option -- %c\n", c);
	ft_printf("usage: ft_ls [-Ralrti] [file ...]\n");
	exit(0);
}

void		check_dir_error(char *path)
{
	if (errno != 20 && errno != 2)
	{
		ft_printf("\nls: %s: %s\n", path, strerror(errno));
	}
}

void		check_file_errors(char **av, int ac)
{
	struct stat	buf;
	int			j;
	int			i;
	int			dbdash;

	i = 1;
	dbdash = 0;
	while (i < ac && ((av[i][0] == '-') && (av[i][1] != '-') && (av[i][1])))
		i++;
	j = i;
	while (i < ac)
	{
		if (!ft_strcmp(av[i], "--") && !dbdash)
		{
			dbdash = 1;
			j++;
		}
		else if ((stat(av[i], &buf)) == -1)
			ft_printf("ft_ls: %s: %s\n", av[i], strerror(errno));
		i++;
	}
}
