/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   add_elem.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/29 07:12:26 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2019/03/06 20:42:00 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

static void		push_back(t_dat *data, t_dat *elem)
{
	if (data->next)
	{
		elem->next = data->next;
		elem->next->prev = elem;
	}
	data->next = elem;
	elem->prev = data;
}

static void		push_front(t_dat *data, t_dat *elem)
{
	if (data->prev)
	{
		elem->prev = data->prev;
		elem->prev->next = elem;
	}
	elem->next = data;
	data->prev = elem;
}

static int		sort_ascii(t_dat *data, t_dat *elem)
{
	if (ft_strcmp(elem->name, data->name) <= 0)
	{
		push_front(data, elem);
		return (1);
	}
	while (data->next && ft_strcmp(elem->name, data->next->name) >= 0)
		data = data->next;
	push_back(data, elem);
	return (0);
}

static int		sort_date(t_dat *data, t_dat *elem)
{
	if (elem->time > data->time || ((elem->time == data->time) &&
									(ft_strcmp(elem->name, data->name) <= 0)))
	{
		push_front(data, elem);
		return (1);
	}
	while ((data->next && ((elem->time < data->next->time) ||
							((elem->time == data->next->time) &&
							(ft_strcmp(elem->name, data->next->name) >= 0)))))
		data = data->next;
	push_back(data, elem);
	return (0);
}

t_dat			*add_elem(t_dat *data, t_dat *elem)
{
	t_dat *start;

	if (!data)
		return (elem);
	start = data;
	if (ft_options("t", DECODE))
	{
		if (sort_date(data, elem))
			return (data->prev);
	}
	else
	{
		if (sort_ascii(data, elem))
			return (data->prev);
	}
	return (start);
}
