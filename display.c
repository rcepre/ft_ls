/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   display.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/05 09:21:02 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2019/03/19 07:40:28 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

static void		forward_run(t_dat *data, t_maxs *maxs)
{
	while (data)
	{
		if (data->name[0] != '.' || ft_options("a", DECODE))
		{
			if (ft_options("l", DECODE))
				display_file_dat_list(data, maxs);
			else
				display_file_dat(data, maxs);
		}
		data = data->next;
	}
}

static void		backward_run(t_dat *data, t_maxs *maxs)
{
	while (data->next)
		data = data->next;
	while (data)
	{
		if (data->name[0] != '.' || ft_options("a", DECODE))
		{
			if (ft_options("l", DECODE))
				display_file_dat_list(data, maxs);
			else
				display_file_dat(data, maxs);
		}
		data = data->prev;
	}
}

void			display_dat_list(t_dat *data, char *path, int nb)
{
	t_maxs		maxs;
	static int	count = 0;

	if (!data)
		return ;
	if (count)
		ft_printf("\n");
	maxs = get_maxs(data);
	if (count && nb)
		ft_printf("%s\n", path);
	if (ft_options("l", DECODE))
		ft_printf("total %d\n", maxs.nb_blocks);
	if (ft_options("r", DECODE))
		backward_run(data, &maxs);
	else
		forward_run(data, &maxs);
	count++;
}
